FROM azul/zulu-openjdk-alpine:8

ENV PORT 8080

EXPOSE 8080

ARG JAR_FILE=target/*.jar

COPY target/timesheet-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-jar","app.jar"]
