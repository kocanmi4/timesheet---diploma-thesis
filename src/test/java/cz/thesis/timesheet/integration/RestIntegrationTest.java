package cz.thesis.timesheet.integration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.thesis.timesheet.TimesheetApplication;
import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Person;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = TimesheetApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
/*@TestPropertySource(
		locations = "classpath:application-integrationtest.properties")*/
public class RestIntegrationTest {
	@Autowired
	private MockMvc mvc;

	@Autowired
	private IPersonRepository dao;

	@After
	public void resetDb() {
		dao.deleteAll();
	}

	@Test
	public void smokeTest() throws Exception {
		Person p = new Person();
		p.setName("Name");
		mvc.perform(post("/person")
				.contentType(MediaType.APPLICATION_JSON).content(toJson(p)));

	}

	static byte[] toJson(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return mapper.writeValueAsBytes(object);
	}
}
