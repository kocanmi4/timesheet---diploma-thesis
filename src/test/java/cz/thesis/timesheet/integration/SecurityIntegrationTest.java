package cz.thesis.timesheet.integration;

import cz.thesis.timesheet.TimesheetApplication;
import cz.thesis.timesheet.service.IPersonService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

//@SpringBootTest
//@AutoConfigureWebTestClient
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
//        classes = TimesheetApplication.class)
//@AutoConfigureMockMvc
//@AutoConfigureTestDatabase
public class SecurityIntegrationTest {

    @Autowired
    private WebTestClient client;
    @Autowired
    private IPersonService personService;

    @BeforeEach
    public void beforeEach() {
//        personService.create()
    }

    @Test
    public void test() {
        String personId = "personId";
        this.client.get().uri("/persons/{personId}", personId)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.OK);
    }
}
