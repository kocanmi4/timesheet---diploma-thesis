package cz.thesis.timesheet.unit;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.service.IPersonService;
import cz.thesis.timesheet.service.PersonService;
import cz.thesis.timesheet.service.mapper.IMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

//@RunWith(SpringRunner.class)
public class PersonServiceTest {
//
//	@TestConfiguration
//	public static class TestContextConfiguration {
//
//		@Bean
//		public IPersonService personService(IPersonRepository repository) {
//			return new PersonService(repository, mapper);
//		}
//	}
//
//	@Autowired
//	private IPersonService service;
//
//	@Autowired
//	private IMapper mapper;
//
//	@MockBean
//	private IPersonRepository dao;
//
//	@Before
//	public void setUp() throws Exception {
//		Person p = new Person();
//		p.setName("Name");
//
//		Mockito.when(dao.findById(p.getId())).thenReturn(Optional.of(p));
//	}
//
//	@Test
//	public void someStupidTest() {
//
//	}
}
