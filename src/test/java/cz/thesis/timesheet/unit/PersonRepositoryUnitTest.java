package cz.thesis.timesheet.unit;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Person;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataMongoTest
public class PersonRepositoryUnitTest {

	@Autowired
	private IPersonRepository dao;

	@Test
	public void whenFindByName_thenReturnEmployee() {
		// given
		Person person = new Person();
		person.setName("Name");


		dao.save(person);

		// when
		Optional<Person> found = dao.findById(person.getId());

		// then
		assertThat(found.get().getName())
				.isEqualTo(person.getName());
	}

	@Autowired MongoTemplate mongoTemplate;

	@DisplayName("given object to save"
			+ " when save object using MongoDB template"
			+ " then object is saved")
	@Test
	public void testMongoDb() {

		// given
		DBObject objectToSave = BasicDBObjectBuilder.start()
				.add("key", "value")
				.get();

		// when
		mongoTemplate.save(objectToSave, "collection");

		// then
		assertThat(mongoTemplate.findAll(DBObject.class, "collection")).extracting("key")
				.containsOnly("value");
	}
}
