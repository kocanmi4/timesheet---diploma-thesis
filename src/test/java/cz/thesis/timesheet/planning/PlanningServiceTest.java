package cz.thesis.timesheet.planning;

import static org.assertj.core.api.Assertions.assertThat;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PlanningServiceTest {

    @Mock
    private IPersonRepository personRepository;
    @InjectMocks
    private PlanningService planningService;

    @Test
    public void testSumOfHours() {
        String id = "some-id";
        Person person = new Person();
        List<Assignment> assignments = new ArrayList<>();
        Assignment a = new Assignment();
        a.setFrom(LocalDate.of(2019, Month.JANUARY, 1));
        a.setTo(LocalDate.of(2019, Month.DECEMBER, 31));
        a.setWorkload(Double.valueOf(20*8));
        assignments.add(a);
        a = new Assignment();
        a.setFrom(LocalDate.of(2019, Month.JUNE, 1));
        a.setTo(LocalDate.of(2019, Month.AUGUST, 31));
        a.setWorkload(Double.valueOf(4*2*4));
        assignments.add(a);
        person.setAssignments(assignments);

        assertThat(YearMonth.of(2019, Month.JANUARY)
                .equals(YearMonth.of(2019, Month.JANUARY))).isTrue();

        when(personRepository.findById(id)).thenReturn(Optional.of(person));

        PersonWorkloadDto result = planningService.showPersonsWorkload(id,
                LocalDate.of(2019, Month.JANUARY, 1), LocalDate.of(2019, Month.DECEMBER, 31));

        assertThat(result.getWorkloadList()).isNotNull().satisfies(value -> {
            assertThat(value.get(0).getWorkload()).isEqualTo(160D);
            assertThat(value.get(1).getWorkload()).isEqualTo(160D);
            assertThat(value.get(6).getWorkload()).isEqualTo(160D + 32D);
            assertThat(value.get(6).getWorkload()).isEqualTo(160D + 32D);
            assertThat(value.get(11).getWorkload()).isEqualTo(160D);
        });
    }
}