package cz.thesis.timesheet.mvc;

import cz.thesis.timesheet.controller.PersonController;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class PersonControllerTestWithBuilder {

    private MockMvc mvc;

    @Mock
    private PersonService service;

    @InjectMocks
    private PersonController controller;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getTest() throws Exception {
        PersonDto p = new PersonDto();
        p.setName("Michal");
        when(service.getAll()).thenReturn(Arrays.asList(p));

        mvc.perform(get("/persons"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(p.getName())));
    }

}
