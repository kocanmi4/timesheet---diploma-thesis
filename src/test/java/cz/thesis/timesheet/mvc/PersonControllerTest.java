package cz.thesis.timesheet.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.thesis.timesheet.controller.PersonController;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.security.MongoUserDetailsService;
import cz.thesis.timesheet.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

	@Autowired
	private MockMvc mvc;
	@Autowired
	private ObjectMapper objectMapper;
	@MockBean
	private MongoUserDetailsService userDetailsService;
	@MockBean
	private PersonService service;


	@Test
	public void getTest() throws Exception {
		PersonDto p = new PersonDto();
		p.setName("Michal");
		given(service.getAll()).willReturn(Arrays.asList(p));

		mvc.perform(get("/persons"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].name", is(p.getName())));
	}

	@Test
	public void create() {

	}

	@Test
	public void whenInputIsInvalid_thenReturnsStatus400() throws Exception {
		PersonDto p = new PersonDto();
		p.setId("noIdShouldBePresent");
		String body = objectMapper.writeValueAsString(p);

		mvc.perform(post("/persons/")
				.contentType("application/json")
				.content(body))
				.andExpect(status().isBadRequest());
	}
}
