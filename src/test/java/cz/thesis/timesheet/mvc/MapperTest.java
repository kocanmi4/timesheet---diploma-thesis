package cz.thesis.timesheet.mvc;

import cz.thesis.timesheet.service.mapper.IMapper;
import cz.thesis.timesheet.service.mapper.MapstructMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MapperTest {

	private final IMapper mapper = Mappers.getMapper(MapstructMapper.class);

	@Test
	public void givenSourceToDestination_whenMaps_thenCorrect() {

	}

	@Test
	public void givenDestinationToSource_whenMaps_thenCorrect() {

	}
}
