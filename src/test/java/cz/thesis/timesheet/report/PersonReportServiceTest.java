package cz.thesis.timesheet.report;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.domain.Timesheet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PersonReportServiceTest {

    @Mock
    private IPersonRepository personRepository;
    @InjectMocks
    private PersonReportService personReportService;

    @Test
    public void testSumOfHours() {
        String id = "some-id";
        Person person = new Person();
        List<Timesheet> timesheets = new ArrayList<>();
        Timesheet t = new Timesheet();
        t.setHourWorked(4.0);
        timesheets.add(t);
        t = new Timesheet();
        t.setHourWorked(3.5);
        timesheets.add(t);
        person.setTimesheets(timesheets);

        when(personRepository.findById(id)).thenReturn(Optional.of(person));

        //PersonReportDto personReportDto = personReportService.getTimesheetReport(id);

        //assertThat(personReportDto.getTotalHoursWorked()).isEqualTo(Double.valueOf(7.5));
    }

}