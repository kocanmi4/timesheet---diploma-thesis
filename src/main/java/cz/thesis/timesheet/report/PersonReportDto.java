package cz.thesis.timesheet.report;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PersonReportDto {

    private String name;

    private List<Project> projects;

    private Double totalHoursWorked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotalHoursWorked() {
        return totalHoursWorked;
    }

    public void setTotalHoursWorked(Double totalHoursWorked) {
        this.totalHoursWorked = totalHoursWorked;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    class Project {
        private String name;

        private Double hoursWorked;

        public Project() {
        }

        public Project(String name, Double hoursWorked) {
            this.name = name;
            this.hoursWorked = hoursWorked;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getHoursWorked() {
            return hoursWorked;
        }

        public void setHoursWorked(Double hoursWorked) {
            this.hoursWorked = hoursWorked;
        }
    }
}
