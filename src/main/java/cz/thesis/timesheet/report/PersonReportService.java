package cz.thesis.timesheet.report;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.domain.Timesheet;
import cz.thesis.timesheet.service.NotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PersonReportService {

    private final IPersonRepository personRepository;

    public PersonReportService(IPersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public PersonReportDto getTimesheetReport(String userId, LocalDate startDate, LocalDate endDate) {
        Person person = personRepository.findById(userId).orElseThrow(() -> {throw new NotFoundException(userId);});
        List<Timesheet> l = person.getTimesheets().stream()
                .filter(t -> startDate.isBefore(t.getDate()) && endDate.isAfter(t.getDate())).collect(Collectors.toList());
        Map<String, Double> report = new HashMap<>();
        for (Timesheet t : l) {
            if (!report.containsKey(t.getProjectId())) {
                report.put(t.getProjectId(), t.getHourWorked());
                continue;
            }
            report.computeIfPresent(t.getProjectId(), (s, v) -> v + t.getHourWorked());
        }
        double sum = l.stream().mapToDouble(x -> x.getHourWorked()).sum();
        PersonReportDto reportDto = new PersonReportDto();
        List<PersonReportDto.Project> projects = new ArrayList<>();
        for (Map.Entry<String, Double> entry : report.entrySet()) {
            String project = entry.getKey();
            Double hours = entry.getValue();
            projects.add(reportDto.new Project(project, hours));
        }
        reportDto.setName(person.getName());
        reportDto.setProjects(projects);
        reportDto.setTotalHoursWorked(sum);
        return reportDto;
//        return new PersonReportDto(person.getName(), null, sum);
    }

}
