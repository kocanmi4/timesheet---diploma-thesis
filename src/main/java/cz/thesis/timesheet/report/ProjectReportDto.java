package cz.thesis.timesheet.report;

import java.util.List;

public class ProjectReportDto {

    private String id;
    private String name;
    private Double hoursWorked;

    private List<Person> persons;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(Double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    class Person {
        private String id;
        private String name;
        private Double hoursWorked;

        public Person() {
        }

        public Person(String id, String name, Double hoursWorked) {
            this.id = id;
            this.name = name;
            this.hoursWorked = hoursWorked;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getHoursWorked() {
            return hoursWorked;
        }

        public void setHoursWorked(Double hoursWorked) {
            this.hoursWorked = hoursWorked;
        }
    }
}
