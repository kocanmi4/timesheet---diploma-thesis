package cz.thesis.timesheet.report;

import cz.thesis.timesheet.dao.IProjectRepository;
import cz.thesis.timesheet.domain.Project;
import cz.thesis.timesheet.domain.Timesheet;
import cz.thesis.timesheet.service.NotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectReportService {

    private final IProjectRepository projectRepository;

    public ProjectReportService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectReportDto getProjectReport(String projectId, LocalDate startDate, LocalDate endDate) {
        Project project = projectRepository.findById(projectId).orElseThrow(() -> {
            throw new NotFoundException();
        });
        List<Timesheet> l = project.getTimesheets().stream()
                .filter(t -> startDate.isBefore(t.getDate()) && endDate.isAfter(t.getDate())).collect(Collectors.toList());
        double sum = l.stream().mapToDouble(timesheet -> timesheet.getHourWorked()).sum();
        Map<String, Double> report = new HashMap<>();
        for (Timesheet t : l) {
            if (!report.containsKey(t.getPersonId())) {
                report.put(t.getPersonId(), t.getHourWorked());
                continue;
            }
            report.computeIfPresent(t.getPersonId(), (s, v) -> v + t.getHourWorked());
        }
        ProjectReportDto reportDto = new ProjectReportDto();
        List<ProjectReportDto.Person> personList = new ArrayList<>();
        for (Map.Entry<String, Double> entry : report.entrySet()) {
            String personId = entry.getKey();
            Double hours = entry.getValue();
            personList.add(reportDto.new Person(personId, null, hours));
        }
        reportDto.setPersons(personList);
        reportDto.setHoursWorked(sum);
        reportDto.setName(project.getName());
        reportDto.setId(project.getId());
        return reportDto;
    }
}
