package cz.thesis.timesheet.report;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping("/reports")
public class ReportsController {

    private final PersonReportService personReportService;
    private final ProjectReportService projectReportService;

    public ReportsController(PersonReportService personReportService, ProjectReportService projectReportService) {
        this.personReportService = personReportService;
        this.projectReportService = projectReportService;
    }

    @GetMapping("/persons/{userId}")
    //@IsAuthorizedUser
    public PersonReportDto reportPerson(@PathVariable String userId,
                                        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
                                        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        //kolik hodin vykazal na jaky projekt
//        System.out.println(principal.getName());
        if(startDate.isPresent() && endDate.isPresent()){
            return personReportService.getTimesheetReport(userId, startDate.get(), endDate.get());
        } else {
            return null;
        }
    }

    @GetMapping("/projects/{projectId}")
    public ProjectReportDto reportProject(@PathVariable String projectId,
                                          @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
                                          @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        //kolik hodin se vykazalo ruznymi lidmi
        if (startDate.isPresent() && endDate.isPresent()) {
            return projectReportService.getProjectReport(projectId, startDate.get(), endDate.get());
        } else {
            return null;
        }
    }
}
