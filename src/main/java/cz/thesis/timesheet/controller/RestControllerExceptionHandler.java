package cz.thesis.timesheet.controller;

import cz.thesis.timesheet.controller.dto.ApiError;
import cz.thesis.timesheet.service.ConflictException;
import cz.thesis.timesheet.service.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {
	//NOT_FOUND 404
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Object> handleNotFoundException(NotFoundException ex) {
		final String errorMsg = "Object not found in database";
		logger.error(errorMsg, ex);
		ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), errorMsg);
		return new ResponseEntity<Object>(apiError, apiError.getStatus());
	}

	@ExceptionHandler(ConflictException.class)
	public ResponseEntity<Object> handleConflictException(ConflictException ex) {
		final String errorMsg = "Conflict event occurred";
		logger.error(errorMsg, ex);
		ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage(), errorMsg);
		return new ResponseEntity<Object>(apiError, apiError.getStatus());
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error(ex.getLocalizedMessage(), ex);
		List<String> errors = new ArrayList<>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		ApiError apiError
				= new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}
//	Map<String, Object> body = new LinkedHashMap<>();
//        body.put("timestamp", new Date());
//        body.put("status", status.value());
//
//	//Get all errors
//	List<String> errors = ex.getBindingResult()
//			.getFieldErrors()
//			.stream()
//			.map(x -> x.getDefaultMessage())
//			.collect(Collectors.toList());
//
//        body.put("errors", errors);
}
