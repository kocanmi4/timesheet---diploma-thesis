package cz.thesis.timesheet.controller;

import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.service.ITimesheetService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/timesheets")
public class TimesheetController {

	private final ITimesheetService service;

	public TimesheetController(ITimesheetService service) {
		this.service = service;
	}

	@GetMapping
	public List<TimesheetDto> getAll(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<LocalDateTime> startDate,
									 @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<LocalDateTime> endDate) {
		if(startDate.isPresent() && endDate.isPresent()) {
			return service.getByDate(startDate.get().toLocalDate(), endDate.get().toLocalDate());
		} else {
			return service.getAll();
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN' and @timesheetIdEvaluator.canAccess(authentication, #id))")
	public TimesheetDto getById(@PathVariable String id) {
		return service.getById(id).get();
	}

	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public TimesheetDto create(@RequestBody TimesheetDto dto) {
		return service.create(dto);
	}

	@PutMapping("/{id}")
	public TimesheetDto updateByPut(@PathVariable String id, @RequestBody TimesheetDto dto) {
		dto.setId(id);
		return service.update(dto);
	}

	//@PatchMapping("/{id}")
//	public TimesheetDto updateByPatch(@PathVariable String id, @RequestBody TimesheetDto dto) {
//		return null;
//	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String id) {
		service.delete(id);
	}
}
