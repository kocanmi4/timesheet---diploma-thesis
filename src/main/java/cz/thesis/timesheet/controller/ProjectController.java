package cz.thesis.timesheet.controller;

import cz.thesis.timesheet.controller.dto.ProjectDto;
import cz.thesis.timesheet.service.IProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectController {

	private final IProjectService service;

	public ProjectController(IProjectService service) {
		this.service = service;
	}

	@GetMapping
	public List<ProjectDto> getAll() {
		return service.getAll();
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAnyRole('[ADMIN, USER]') and @timesheetIdEvaluator.isOnProject(authentication, #id)")
	public ProjectDto getById(@PathVariable String id) {
		return service.getById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ADMIN')")
	public ProjectDto create(@RequestBody ProjectDto dto) {
		return service.create(dto);
	}

	@PutMapping("/{id}")
	public ProjectDto updateByPut(@PathVariable String id, @RequestBody ProjectDto dto) {
		dto.setId(id);
		return service.update(dto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String id) {
		service.delete(id);
	}
}
