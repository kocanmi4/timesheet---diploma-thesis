package cz.thesis.timesheet.controller.dto;

import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Competence;

import javax.validation.constraints.Null;
import java.util.List;
import java.util.Set;

public class PersonDetailDto {

    private String id;
    private String name;
    @Null
    private String username;
    private Set<Competence> competenceSet;
    private List<Assignment> assignments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Competence> getCompetenceSet() {
        return competenceSet;
    }

    public void setCompetenceSet(Set<Competence> competenceSet) {
        this.competenceSet = competenceSet;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }
}
