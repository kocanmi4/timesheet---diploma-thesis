package cz.thesis.timesheet.controller.dto;

import cz.thesis.timesheet.domain.Competence;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class PersonCreateDto {

    @NotBlank(message = "Name is mandatory")
    @NotNull
    private String name;

    @NotNull
    @NotEmpty
    private String username;
    @NotNull
    @NotEmpty
    private String password;

    private Set<Competence> competenceSet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Competence> getCompetenceSet() {
        return competenceSet;
    }

    public void setCompetenceSet(Set<Competence> competenceSet) {
        this.competenceSet = competenceSet;
    }
}
