package cz.thesis.timesheet.controller.dto;

import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Competence;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Set;

public class PersonDto {

	private String id;
	@NotBlank(message = "Name is mandatory")
	@NotNull
	private String name;

	@Null
	private String username;

	private Set<Competence> competenceSet;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Competence> getCompetenceSet() {
		return competenceSet;
	}

	public void setCompetenceSet(Set<Competence> competenceSet) {
		this.competenceSet = competenceSet;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
