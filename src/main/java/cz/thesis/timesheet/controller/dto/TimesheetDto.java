package cz.thesis.timesheet.controller.dto;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

public class TimesheetDto {

	private String id;
	private LocalDate date;
	private Double hourWorked;
	private String state;
	private String activity;
	private String note;

	@NotBlank(message = "User is mandatory")
	private String personId;
	@NotBlank(message = "Project is mandatory")
	private String projectId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Double getHourWorked() {
		return hourWorked;
	}

	public void setHourWorked(Double hourWorked) {
		this.hourWorked = hourWorked;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}
