package cz.thesis.timesheet.controller;

import cz.thesis.timesheet.security.MongoUserDetailsService;
import cz.thesis.timesheet.security.User;
import cz.thesis.timesheet.security.UsersRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersRepository usersRepository;
    private final MongoUserDetailsService userDetailsService;

    public UsersController(UsersRepository usersRepository, MongoUserDetailsService userDetailsService) {
        this.usersRepository = usersRepository;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getUsers() {
        return usersRepository.findAll();
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('[ADMIN, MANAGER]')")
    public User createUser(@RequestBody User user) {
        return null;//userDetailsService.createUser(user);
    }

    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ADMIN') and @timesheetIdEvaluator.isOnProject(authentication, #username)")
    public User testAuth(@PathVariable String username) {
        return usersRepository.findByUsername(username).orElse(new User());
    }

}
