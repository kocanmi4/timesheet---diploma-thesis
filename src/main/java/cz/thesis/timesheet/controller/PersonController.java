package cz.thesis.timesheet.controller;

import cz.thesis.timesheet.controller.dto.PersonCreateDto;
import cz.thesis.timesheet.controller.dto.PersonDetailDto;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.service.IPersonService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/persons")
public class PersonController {

	private final IPersonService service;

	public PersonController(IPersonService service) {
		this.service = service;
	}

	@GetMapping(produces = "application/json")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER', 'USER')")
	public List<PersonDto> getAll() {
		return service.getAll();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER') or #id == authentication.name")
	public PersonDetailDto getById(@PathVariable String id) {
		return service.getById(id);
	}

	@GetMapping(value = "/{id}/timesheet", produces = "application/json")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER') or #id == authentication.name")
	public List<TimesheetDto> getTimesheetsByPersonId(@PathVariable String id) {
		return service.getPersonsTimesheets(id);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ADMIN')")
	public PersonDto create(@Valid @RequestBody PersonCreateDto dto) {
		//check username
		return service.createNewUser(dto);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER') or #id == authentication.name")
	public PersonDetailDto updateByPut(@PathVariable String id, @Valid @RequestBody PersonDetailDto dto) {
		dto.setId(id);
		return service.update(dto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasRole('ADMIN')")
	public void delete(@PathVariable String id) {
		service.delete(id);
	}
}
