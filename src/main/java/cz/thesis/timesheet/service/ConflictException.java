package cz.thesis.timesheet.service;

public class ConflictException extends RuntimeException {
    public ConflictException() {}

    public ConflictException(String msg) {
        super(msg);
    }
}
