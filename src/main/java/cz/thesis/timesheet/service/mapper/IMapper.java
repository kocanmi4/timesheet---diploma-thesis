package cz.thesis.timesheet.service.mapper;

import cz.thesis.timesheet.controller.dto.PersonDetailDto;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.controller.dto.ProjectDto;
import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.domain.Project;
import cz.thesis.timesheet.domain.Timesheet;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;


public interface IMapper {

	PersonDto personToPersonDto(Person p);
	Person personDtoToPerson(PersonDto p);

	PersonDetailDto personToPersonDetailDto(Person p);
	Person personDetailDtoToPerson(PersonDetailDto p);

	ProjectDto projectToProjectDto(Project p);
	Project projectDtoToProject(ProjectDto p);

	TimesheetDto timesheetToTimesheetDto(Timesheet t);
	Timesheet timesheetDtoToTimesheet(TimesheetDto t);
}
