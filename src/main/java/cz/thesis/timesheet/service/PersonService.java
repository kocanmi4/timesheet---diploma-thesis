package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.PersonCreateDto;
import cz.thesis.timesheet.controller.dto.PersonDetailDto;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.dao.IProjectRepository;
import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.domain.Project;
import cz.thesis.timesheet.domain.Timesheet;
import cz.thesis.timesheet.service.mapper.IMapper;
import cz.thesis.timesheet.service.mapper.MapstructMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static cz.thesis.timesheet.security.SecurityRolesAuthorities.ROLE_USER;

@Service
public class PersonService implements IPersonService {

	private static final String PASSWORD_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final int PASSWORD_LENGTH = 6;
	private static final SecureRandom RANDOM = new SecureRandom();

	private final IPersonRepository dao;
	private final IProjectRepository projectRepository;
	private final IMapper mapper = Mappers.getMapper( MapstructMapper.class );
	private final PasswordEncoder passwordEncoder;

	public PersonService(IPersonRepository dao, IProjectRepository projectRepository, PasswordEncoder passwordEncoder) {
		this.dao = dao;
		this.projectRepository = projectRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public List<PersonDto> getAll() {
		List<PersonDto> l = new ArrayList<>();
		dao.findAll().stream().map(mapper::personToPersonDto).forEach(l::add);
		return l;
	}

	@Override
	public PersonDetailDto getById(String id) {
		Optional<Person> optional = dao.findById(id);
		if (optional.isPresent()) {
			return mapper.personToPersonDetailDto(optional.get());
		} else {
			throw new NotFoundException("Person with id " + id + " not found");
		}
	}

	@Override
	public PersonDto create(PersonDto p) {
		Person person = dao.save(mapper.personDtoToPerson(p));
		return mapper.personToPersonDto(person);
	}

	@Override
	public PersonDto createNewUser(PersonCreateDto p) {
		if (dao.findByUsername(p.getUsername()).isPresent()) {
			throw new ConflictException("Username " + p.getUsername() + " already exists");
		}
		Person person = new Person(p.getName(), p.getUsername(), passwordEncoder.encode(p.getPassword()), ROLE_USER);
		person.setCompetenceSet(p.getCompetenceSet());
		return mapper.personToPersonDto(dao.save(person));
	}

	private String generatePassword() {
		StringBuilder sb = new StringBuilder(PASSWORD_LENGTH);
		IntStream.range(0, PASSWORD_LENGTH).forEach(i ->
				sb.append(PASSWORD_CHARS.charAt(RANDOM.nextInt(PASSWORD_CHARS.length()))));
		for (int i = 0; i < PASSWORD_LENGTH; i++) {
			sb.append(PASSWORD_CHARS.charAt(RANDOM.nextInt(PASSWORD_CHARS.length())));
		}
		return sb.toString();
	}

	@Override
	public PersonDetailDto update(PersonDetailDto p) {
		Person orig = dao.findById(p.getId()).orElseThrow(() -> new NotFoundException(p.getId()));
		Person person = mapper.personDetailDtoToPerson(p);
		validateAssignments(person.getAssignments());
		person.setUsername(orig.getUsername());
		person.setPassword(orig.getPassword());
		return mapper.personToPersonDetailDto(dao.save(person));
	}

	private void validateAssignments(List<Assignment> assignments) {
//		Iterator<String> iterator = assignments.stream().map(assignment -> assignment.getProjectId()).iterator();
//		Iterable<String> iterable = () -> assignments.stream().map(assignment -> assignment.getProjectId()).iterator();
//		Iterable<Project> allById = projectRepository.findAllById(iterable);
//		if (StreamSupport.stream(allById.spliterator(), false).count() < assignments.size()) {
//			throw new NotFoundException();
//		}
//		for (Project project : allById) {
//			//...
//		}
		for (Assignment assignment : assignments) {
			String projectName = projectRepository.findById(assignment.getProjectId()).map(project -> project.getName())
					.orElseThrow(() -> new NotFoundException());
			assignment.setProjectName(projectName);
		}
	}

	@Override
	public void delete(String id) {
		dao.deleteById(id);
	}

	@Override
	public List<TimesheetDto> getPersonsTimesheets(String id) {
		List<Timesheet> l = dao.findById(id).orElseThrow(() -> new NotFoundException()).getTimesheets();
		return l.stream().map(mapper::timesheetToTimesheetDto).collect(Collectors.toList());
	}

	@Override
	public void addTimesheet(Timesheet timesheet, String personId) {
		Person p = dao.findById(personId).orElseThrow(() -> new NotFoundException());
		if (p.getTimesheets() == null) {
			p.setTimesheets(new ArrayList<>());
		}
		p.getTimesheets().add(timesheet);
		dao.save(p);
	}
}
