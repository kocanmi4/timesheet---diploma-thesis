package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.PersonCreateDto;
import cz.thesis.timesheet.controller.dto.PersonDetailDto;
import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.domain.Timesheet;

import javax.annotation.Nonnull;
import java.util.List;

public interface IPersonService {

	@Nonnull
	List<PersonDto> getAll();

	@Nonnull
	PersonDetailDto getById(String id);

	@Nonnull
	PersonDto create(PersonDto p);

	@Nonnull
	PersonDetailDto update(PersonDetailDto p);

	@Nonnull
	PersonDto createNewUser(PersonCreateDto p);

	void delete(String id);

    void addTimesheet(Timesheet timesheet, String personId);

    List<TimesheetDto> getPersonsTimesheets(String id);
}
