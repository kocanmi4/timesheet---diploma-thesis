package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.domain.Timesheet;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ITimesheetService {

	@Nonnull
	List<TimesheetDto> getAll();

	@Nonnull
	Optional<TimesheetDto> getById(String id);

	@Nonnull
	TimesheetDto create(TimesheetDto t);

	@Nonnull
	TimesheetDto update(TimesheetDto t);

	void delete(String id);

    List<TimesheetDto> getByDate(LocalDate startDate, LocalDate endDate);
}
