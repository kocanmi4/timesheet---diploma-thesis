package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.ProjectDto;
import cz.thesis.timesheet.domain.Project;
import cz.thesis.timesheet.domain.Timesheet;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

public interface IProjectService {

	@Nonnull
	List<ProjectDto> getAll();

	@Nonnull
	Optional<ProjectDto> getById(String id);

	@Nonnull
	ProjectDto create(ProjectDto p);

	@Nonnull
	ProjectDto update(ProjectDto p);

	void delete(String id);

    void addTimesheet(Timesheet timesheet, String projectId);
}
