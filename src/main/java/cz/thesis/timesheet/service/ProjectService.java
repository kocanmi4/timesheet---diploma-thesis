package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.ProjectDto;
import cz.thesis.timesheet.dao.IProjectRepository;
import cz.thesis.timesheet.domain.Project;
import cz.thesis.timesheet.domain.Timesheet;
import cz.thesis.timesheet.service.mapper.IMapper;
import cz.thesis.timesheet.service.mapper.MapstructMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

	private final IProjectRepository dao;
	private final IMapper mapper = Mappers.getMapper( MapstructMapper.class );

	public ProjectService(IProjectRepository dao) {
		this.dao = dao;
	}

	@Override
	public List<ProjectDto> getAll() {
		List<ProjectDto> l = new ArrayList<>();
		dao.findAll().stream().map(mapper::projectToProjectDto).forEach(l::add);
		return l;
	}

	@Override
	public Optional<ProjectDto> getById(String id) {
		Optional<Project> optional = dao.findById(id);
		if (optional.isPresent()) {
			Project project = optional.get();
			return Optional.of(mapper.projectToProjectDto(project));
		} else {
			return Optional.empty();
		}
//		return optional.orElseThrow(() -> new NotFoundException());
	}

	@Override
	public ProjectDto create(ProjectDto p) {
		return mapper.projectToProjectDto(dao.save(mapper.projectDtoToProject(p)));
	}

	@Override
	public ProjectDto update(ProjectDto p) {
		if(!dao.existsById(p.getId()))
			throw new NotFoundException(p.getId());
		return mapper.projectToProjectDto(dao.save(mapper.projectDtoToProject(p)));
	}

	@Override
	public void delete(String id) {
		dao.deleteById(id);
	}

	@Override
	public void addTimesheet(Timesheet timesheet, String projectId) {
		Project p = dao.findById(projectId).orElseThrow(() -> new NotFoundException());
		if (p.getTimesheets() == null) {
			p.setTimesheets(new ArrayList<>());
		}
		p.getTimesheets().add(timesheet);
		dao.save(p);
	}
}
