package cz.thesis.timesheet.service;

import cz.thesis.timesheet.controller.dto.TimesheetDto;
import cz.thesis.timesheet.dao.ITimesheetRepository;
import cz.thesis.timesheet.domain.Timesheet;
import cz.thesis.timesheet.service.mapper.IMapper;
import cz.thesis.timesheet.service.mapper.MapstructMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TimesheetService implements ITimesheetService {

	private final ITimesheetRepository dao;
	private final IPersonService personService;
	private final IProjectService projectService;
	private final IMapper mapper = Mappers.getMapper( MapstructMapper.class );

	public TimesheetService(ITimesheetRepository dao, IPersonService personService, IProjectService projectService) {
		this.dao = dao;
		this.personService = personService;
		this.projectService = projectService;
	}

	@Override
	public List<TimesheetDto> getAll() {
		List<TimesheetDto> l = new ArrayList<>();
		dao.findAll().stream().map(mapper::timesheetToTimesheetDto).forEach(l::add);
		return l;
	}

	@Override
	public Optional<TimesheetDto> getById(String id) {
		Optional<Timesheet> optional = dao.findById(id);
		if (optional.isPresent()) {
			return Optional.of(mapper.timesheetToTimesheetDto(optional.get()));
		} else {
			return Optional.empty();
		}
//		return optional.orElseThrow(() -> new NotFoundException());
	}

	@Override
	public TimesheetDto create(TimesheetDto t) {
//		log.info("Saving Timesheet '{}', Project id '{}' and Person id '{}'", t.getId(), t.getProjectId(), t.getPersonId());
		Timesheet timesheet = mapper.timesheetDtoToTimesheet(t);
		timesheet = dao.save(timesheet);
		personService.addTimesheet(timesheet, t.getPersonId());
		projectService.addTimesheet(timesheet, t.getProjectId());
		System.out.println(timesheet);
		return mapper.timesheetToTimesheetDto(timesheet);
	}

	@Override
	public TimesheetDto update(TimesheetDto t) {
		if(!dao.existsById(t.getId())) throw new NotFoundException();
		return mapper.timesheetToTimesheetDto(dao.save(mapper.timesheetDtoToTimesheet(t)));
	}

	@Override
	public void delete(String id) {
		dao.deleteById(id);
	}

	@Override
	public List<TimesheetDto> getByDate(LocalDate startDate, LocalDate endDate) {
		return dao.findAllByDateBetween(startDate, endDate)
				.stream().map(mapper::timesheetToTimesheetDto).collect(Collectors.toList());
	}
	//	private TimesheetDto mapToDto(Timesheet t) {
//		TimesheetDto dto = new TimesheetDto();
//		dto.setId(t.getId());
//		dto.setDate(t.getDate());
//		dto.setNote(t.getNote());
//		return dto;
//	}
//
//	private Timesheet mapFromDto(TimesheetDto dto) {
//		Timesheet t = new Timesheet();
//		t.setId(dto.getId());
//		t.setDate(dto.getDate());
//		t.setNote(t.getNote());
//
//		t.setPersonId(dto.getPerson().getId());
//		t.setProjectId(dto.getProject().getId());
//		return t;
//	}
}
