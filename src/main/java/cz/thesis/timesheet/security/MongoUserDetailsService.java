package cz.thesis.timesheet.security;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Person;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MongoUserDetailsService implements UserDetailsService {

    private final IPersonRepository usersRepository;

    public MongoUserDetailsService(IPersonRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Person p = usersRepository.findByUsername(s).orElseThrow(() -> {throw new UsernameNotFoundException("User not found");});
        List<GrantedAuthority> roles = Optional.ofNullable(p.getAuthorities()).orElse(Collections.emptySet()).stream()
                .map((role) -> new SimpleGrantedAuthority(role.toUpperCase())).collect(Collectors.toList());
//        roles.addAll(p.getAuthorities().stream()
//                .map(authority -> new SimpleGrantedAuthority(SecurityRolesAuthorities.ASSIGNMENT_PREFIX + authority)).collect(Collectors.toList()));
        roles.addAll(Optional.ofNullable(p.getAssignments()).orElse(Collections.emptyList()).stream().map(this::getProjectId)
                .map(projectId ->
                    new SimpleGrantedAuthority(SecurityRolesAuthorities.ASSIGNMENT_PREFIX + projectId)
                ).collect(Collectors.toList()));
        return buildUser(p, roles);
    }

    private String getProjectId(Assignment assignment) {
        return assignment.getProjectId();
    }

    private UserDetails buildUser(Person user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getId(), user.getPassword(), authorities);
    }
}
