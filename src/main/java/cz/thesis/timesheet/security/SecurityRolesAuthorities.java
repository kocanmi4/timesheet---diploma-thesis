package cz.thesis.timesheet.security;

public class SecurityRolesAuthorities {

    public static final String ASSIGNMENT_PREFIX = "ASSIGNMENT_";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String ROLE_USER = "ROLE_USER";
}
