package cz.thesis.timesheet.security;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import java.net.Authenticator;

import static cz.thesis.timesheet.security.SecurityRolesAuthorities.ASSIGNMENT_PREFIX;

public class TimesheetIdEvaluator {

    public boolean canAccess(Authenticator authenticator, String id) {
        return true;
    }

    public boolean ownThisResource(Authenticator authenticator, String resourceId) {
        return false;
    }

    public boolean isOnProject(Authentication authentication, String projectId) {
        return authentication.getAuthorities().stream()
                .filter(authority -> authority.getAuthority().startsWith(ASSIGNMENT_PREFIX))
                .map(authority -> {
                    String id = authority.getAuthority().substring(ASSIGNMENT_PREFIX.length());
                    return id.equals(projectId);
                })
                .anyMatch(Boolean.TRUE::equals);
//                .orElse(false);
    }
}
