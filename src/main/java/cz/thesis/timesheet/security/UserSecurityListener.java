package cz.thesis.timesheet.security;

import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.dao.IProjectRepository;
import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.domain.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class UserSecurityListener implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private IPersonRepository personRepository;
    @Autowired
    private IProjectRepository projectRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        if(personRepository.count() == 0) {
            Person person = new Person("admin", "admin", passwordEncoder.encode("admin"), SecurityRolesAuthorities.ROLE_ADMIN, "admin");
//            person.setRoles(Stream.of(SecurityRolesAuthorities.ROLE_ADMIN).collect(Collectors.toSet()));
            personRepository.save(person);
        }

        if(projectRepository.count() == 0) {
            Project project = new Project();
            project.setName("project");
            projectRepository.save(project);
        }

        Person person = personRepository.findByUsername("admin").orElse(new Person());
        Assignment assignment = new Assignment();
        String projectId = projectRepository.findAll().stream().findFirst().map(project -> project.getId()).orElse(null);
        assignment.setProjectId(projectId);
        person.setAssignments(Arrays.asList(assignment));
        personRepository.save(person);
    }
}
