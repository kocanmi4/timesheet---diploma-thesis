package cz.thesis.timesheet.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.time.LocalDate;

@Document("timesheets")
public class Timesheet {

	@Id
	private String id;
	private LocalDate date;
	private Double hourWorked;
	@Enumerated(EnumType.STRING)
	private TimesheetStates state;
	private String activity;
	private String note;
	private String projectId;
	private String personId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Double getHourWorked() {
		return hourWorked;
	}

	public void setHourWorked(Double hourWorked) {
		this.hourWorked = hourWorked;
	}

	public TimesheetStates getState() {
		return state;
	}

	public void setState(TimesheetStates state) {
		this.state = state;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	@Override
	public String toString() {
		return "Timesheet{" +
				"id='" + id + '\'' +
				", date=" + date +
				", hour=" + hourWorked +
				", state=" + state +
				", activity='" + activity + '\'' +
				", note='" + note + '\'' +
				'}';
	}
}
