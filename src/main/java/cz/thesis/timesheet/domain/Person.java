package cz.thesis.timesheet.domain;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Document("persons")
public class Person {

	@Id
	private String id;
	private String name;

	private Set<Competence> competenceSet;

	@Indexed(unique=true)
	private String username;
	private String password;

//	private Set<String> roles;

	private Set<String> authorities;

	public Person() {}

	public Person(String name, String username, String password, String... authorities) {
		this.name = name;
		this.username = username;
		this.password = password;
		this.authorities = Stream.of(authorities).collect(Collectors.toSet());
	}

	@DBRef @Field("timesheets")
	private List<Timesheet> timesheets;

	private List<Assignment> assignments;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Timesheet> getTimesheets() {
		return timesheets;
	}

	public void setTimesheets(List<Timesheet> timesheets) {
		this.timesheets = timesheets;
	}

	public Set<Competence> getCompetenceSet() {
		return competenceSet;
	}

	public void setCompetenceSet(Set<Competence> competenceSet) {
		this.competenceSet = competenceSet;
	}

	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public Set<String> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<String> roles) {
//		this.roles = roles;
//	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<String> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String toString() {
		return "Person{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", competenceSet=" + competenceSet +
				", timesheets=" + timesheets +
				'}';
	}
}
