package cz.thesis.timesheet.domain;

public enum TimesheetStates {
	OPEN, APPROVED;
}
