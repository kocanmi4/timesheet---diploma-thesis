package cz.thesis.timesheet.domain;

public class Competence {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
