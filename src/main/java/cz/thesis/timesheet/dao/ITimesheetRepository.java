package cz.thesis.timesheet.dao;

import cz.thesis.timesheet.domain.Timesheet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface ITimesheetRepository extends MongoRepository<Timesheet, String> {

    List<Timesheet> findAllByDate(LocalDate date);

    List<Timesheet> findAllByDateBetween(LocalDate dateGT, LocalDate dateLT);

}
