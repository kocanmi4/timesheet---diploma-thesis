package cz.thesis.timesheet.dao;

import cz.thesis.timesheet.domain.Project;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

public interface IProjectRepository extends MongoRepository<Project, String> {
}
