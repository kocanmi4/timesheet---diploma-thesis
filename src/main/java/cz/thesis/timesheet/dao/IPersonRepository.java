package cz.thesis.timesheet.dao;

import cz.thesis.timesheet.domain.Competence;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.security.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IPersonRepository extends MongoRepository<Person, String> {

    List<Person> findAllByCompetenceSet(Competence competence);

    Optional<Person> findByUsername(String username);

}
