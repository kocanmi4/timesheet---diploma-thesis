package cz.thesis.timesheet.planning;

import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.dao.IPersonRepository;
import cz.thesis.timesheet.domain.Assignment;
import cz.thesis.timesheet.domain.Competence;
import cz.thesis.timesheet.domain.Person;
import cz.thesis.timesheet.service.NotFoundException;
import cz.thesis.timesheet.service.mapper.IMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class PlanningService {

    private final IPersonRepository personRepository;
    private final IMapper mapper;

    public PlanningService(IPersonRepository personRepository, IMapper mapper) {
        this.personRepository = personRepository;
        this.mapper = mapper;
    }

    public List<PersonDto> recommend(Competence competence) {
        return personRepository.findAllByCompetenceSet(competence)
                .stream().map(mapper::personToPersonDto).collect(Collectors.toList());
    }

    public PersonWorkloadDto showPersonsWorkload(String id, LocalDate startDate, LocalDate endDate) {
        Map<YearMonth, Double> plan = prepareMap(YearMonth.from(startDate), YearMonth.from(endDate));
        Person person = personRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(id);
        });
        List<Assignment> assignments = person.getAssignments();
//        assignments.stream().map(this::findWorkload)
        PersonWorkloadDto report = new PersonWorkloadDto();
        List<PersonWorkloadDto.MonthWorkload> resultList = new ArrayList<>();
        plan.forEach((yearMonth, aDouble) -> {
            double workload = assignments.stream().filter(a -> {
                YearMonth from = YearMonth.from(a.getFrom());
                YearMonth to = YearMonth.from(a.getTo());
                return (from.isBefore(yearMonth) || from.equals(yearMonth))
                        && (to.isAfter(yearMonth) || to.equals(yearMonth));
            }).mapToDouble(Assignment::getWorkload).sum();
            resultList.add(report.new MonthWorkload(yearMonth, null, workload));
        });
        report.setId(person.getId());
        report.setName(person.getName());
        report.setWorkloadList(resultList);
        return report;
    }

    private Pair findWorkload(Assignment assignment) {
        return null;
    }

    private Map<YearMonth, Double> prepareMap(YearMonth start, YearMonth end) {
        long months = ChronoUnit.MONTHS.between(start, end);
//        Period p = Period.between(start, end);
        Map<YearMonth, Double> map = new LinkedHashMap<>();
        IntStream.range(0, (int)months).forEach(i -> {
            map.put(start.plusMonths(i), Double.valueOf(0));
        });
        map.put(end, Double.valueOf(0));
        return map;
    }

    private class Pair {
        public YearMonth date;
        public Double workload;

        public Pair(YearMonth date, Double workload) {
            this.date = date;
            this.workload = workload;
        }
    }
}
