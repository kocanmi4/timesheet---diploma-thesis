package cz.thesis.timesheet.planning;

import cz.thesis.timesheet.controller.dto.PersonDto;
import cz.thesis.timesheet.domain.Competence;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/planning")
public class PlanningController {

    private final PlanningService planningService;

    public PlanningController(PlanningService planningService) {
        this.planningService = planningService;
    }

    @GetMapping
    public List<PersonDto> recommendPersons(@RequestParam Optional<String> competence) {
        Competence c = new Competence();
        c.setName(competence.orElse(""));
        return planningService.recommend(c);
    }

    @GetMapping("/{id}")
    public PersonWorkloadDto showFutureWorkload(@PathVariable String id,
                                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
                                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        if (startDate.isPresent() && endDate.isPresent()) {
            return planningService.showPersonsWorkload(id, startDate.get(), endDate.get());
        } else {
            return null;
        }
    }
}
