package cz.thesis.timesheet.planning;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

public class PersonWorkloadDto {

    private String id;
    private String name;

    private List<MonthWorkload> workloadList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MonthWorkload> getWorkloadList() {
        return workloadList;
    }

    public void setWorkloadList(List<MonthWorkload> workloadList) {
        this.workloadList = workloadList;
    }

    class MonthWorkload {
        private YearMonth date;
        private Integer plannedHours;
        private Double workload;

        public MonthWorkload() {
        }

        public MonthWorkload(YearMonth date, Integer plannedHours, Double workload) {
            this.date = date;
            this.plannedHours = plannedHours;
            this.workload = workload;
        }

        public YearMonth getDate() {
            return date;
        }

        public void setDate(YearMonth date) {
            this.date = date;
        }

        public Integer getPlannedHours() {
            return plannedHours;
        }

        public void setPlannedHours(Integer plannedHours) {
            this.plannedHours = plannedHours;
        }

        public Double getWorkload() {
            return workload;
        }

        public void setWorkload(Double workload) {
            this.workload = workload;
        }
    }
}
