package cz.thesis.timesheet.config;

import cz.thesis.timesheet.security.TimesheetIdEvaluator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig /*extends GlobalMethodSecurityConfiguration*/ {

//    @Override
//    protected MethodSecurityExpressionHandler createExpressionHandler() {
//        return new DefaultMethodSecurityExpressionHandler();
//    }

    @Bean
    public TimesheetIdEvaluator timesheetIdEvaluator() {
        return new TimesheetIdEvaluator();
    }
}
